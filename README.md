# IntFinder database
This is the repository for the IntFinder database. The IntFinder database is a curated database of integrons.

## Documentation
This repository contains the IntFinder database. The integrons in this database are named as stated on INTEGRALL http://integrall.bio.ua.pt/.


## Content of the repository
- db1.fsa - template sequences being used for the alignment in IntFinder.

- notes.txt - curated list of integron names, accession numbers,
genes and their positions.

- config - configuration file.

- INSTALL.py - a python script.


## Installation:
Download and install IntFinder database
```bash
# Go to the directory where you want to store the intfinder database
cd /path/to/some/dir
# Clone database from git repository (master branch)
git clone https://bitbucket.org/genomicepidemiology/intfinder_db.git
cd intfinder_db
INTFINDER_DB=$(pwd)
# Install intfinder database with executable kma_index program
python3 INSTALL.py
```


## When using the method please cite:
Torres-Elizalde, L., Ortega-Paredes, D., Loaiza, K., Fernández-Moreira, E., & Larrea-Álvarez, M. (2021). In Silico Detection of Antimicrobial Resistance Integrons in Salmonella enterica Isolates from Countries of the Andean Community. Antibiotics, 10(11), Article 11. https://doi.org/10.3390/antibiotics10111388


#### References:
- Torres-Elizalde, L., Ortega-Paredes, D., Loaiza, K., Fernández-Moreira, E., & Larrea-Álvarez, M. (2021). In Silico Detection of Antimicrobial Resistance Integrons in Salmonella enterica Isolates from Countries of the Andean Community. Antibiotics, 10(11), Article 11. https://doi.org/10.3390/antibiotics10111388

- Torres, L., Loaiza, K., Larrea-Álvarez, M., & Ortega-Paredes, D. (2021). Insights into the relationship between Salmonella enterica serovars and resistance integrons in the Andean Region using a novel bioinformatics tool: IntFinder. https://doi.org/10.13140/RG.2.2.25108.58241

- Torres, L., Loaiza, K., Larrea-Álvarez, M., & Ortega-Paredes, D. (2021). Relación entre serotipos de Salmonella enterica e integrones de resistencia en la Región Andina utilizando una novedosa herramienta bioinformática: IntFinder.

- Loaiza, K., Ortega-Paredes, D., Johansson, M., Florensa, A., Lund, O., & Bortolaia, V. (2020). IntFinder: A freely-available user-friendly web tool for detection of class 1 integrons in next-generation sequencing data using k-mer alignment. https://doi.org/10.13140/RG.2.2.17974.11844

- Clausen, P. T. L. C., Aarestrup, F. M., & Lund, O. (2018). Rapid and precise alignment of raw reads against redundant databases with KMA. BMC Bioinformatics, 19(1), 307. https://doi.org/10.1186/s12859-018-2336-6


## License
Copyright (c) 2020, Karen Loaiza, Technical University of Denmark All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
